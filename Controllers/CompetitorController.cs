using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ASPCompetitionManager.DAL;
using ASPCompetitionManager.Models;

namespace ASPCompetitionManager.Controllers
{
    public class CompetitorController : Controller
    {
        private readonly CompetitionContext _context;

        public CompetitorController(CompetitionContext context)
        {
            _context = context;
        }

        // GET: Competitor
        public async Task<IActionResult> Index()
        {
            var competitionContext = _context.Competitor.Include(c => c.Coach).Include(c => c.Team);
            return View(await competitionContext.ToListAsync());
        }

        // GET: Competitor/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var competitor = await _context.Competitor
                .Include(c => c.Coach)
                .Include(c => c.Team)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (competitor == null)
            {
                return NotFound();
            }

            return View(competitor);
        }

        // GET: Competitor/Create
        public IActionResult Create()
        {
            ViewData["CoachId"] = new SelectList(_context.Coach, "Id", "Id");
            ViewData["TeamId"] = new SelectList(_context.Team, "Id", "Id");
            return View();
        }

        // POST: Competitor/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CoachId,TeamId,Id,FirstName,LastName,Gender,Age")] Competitor competitor)
        {
            if (ModelState.IsValid)
            {
                _context.Add(competitor);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CoachId"] = new SelectList(_context.Coach, "Id", "Id", competitor.CoachId);
            ViewData["TeamId"] = new SelectList(_context.Team, "Id", "Id", competitor.TeamId);
            return View(competitor);
        }

        // GET: Competitor/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var competitor = await _context.Competitor.FindAsync(id);
            if (competitor == null)
            {
                return NotFound();
            }
            ViewData["CoachId"] = new SelectList(_context.Coach, "Id", "Id", competitor.CoachId);
            ViewData["TeamId"] = new SelectList(_context.Team, "Id", "Id", competitor.TeamId);
            return View(competitor);
        }

        // POST: Competitor/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CoachId,TeamId,Id,FirstName,LastName,Gender,Age")] Competitor competitor)
        {
            if (id != competitor.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(competitor);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CompetitorExists(competitor.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CoachId"] = new SelectList(_context.Coach, "Id", "Id", competitor.CoachId);
            ViewData["TeamId"] = new SelectList(_context.Team, "Id", "Id", competitor.TeamId);
            return View(competitor);
        }

        // GET: Competitor/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var competitor = await _context.Competitor
                .Include(c => c.Coach)
                .Include(c => c.Team)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (competitor == null)
            {
                return NotFound();
            }

            return View(competitor);
        }

        // POST: Competitor/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var competitor = await _context.Competitor.FindAsync(id);
            _context.Competitor.Remove(competitor);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CompetitorExists(int id)
        {
            return _context.Competitor.Any(e => e.Id == id);
        }
    }
}
