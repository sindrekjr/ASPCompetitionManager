using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ASPCompetitionManager.DAL;
using ASPCompetitionManager.Models;

namespace ASPCompetitionManager.Controllers
{
    public class CompetitorSkillController : Controller
    {
        private readonly CompetitionContext _context;

        public CompetitorSkillController(CompetitionContext context)
        {
            _context = context;
        }

        // GET: CompetitorSkill
        public async Task<IActionResult> Index()
        {
            var competitionContext = _context.CompetitorSkill.Include(c => c.Competitor).Include(c => c.Skill);
            return View(await competitionContext.ToListAsync());
        }

        // GET: CompetitorSkill/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var competitorSkill = await _context.CompetitorSkill
                .Include(c => c.Competitor)
                .Include(c => c.Skill)
                .FirstOrDefaultAsync(m => m.CompetitorId == id);
            if (competitorSkill == null)
            {
                return NotFound();
            }

            return View(competitorSkill);
        }

        // GET: CompetitorSkill/Create
        public IActionResult Create()
        {
            ViewData["CompetitorId"] = new SelectList(_context.Competitor, "Id", "Id");
            ViewData["SkillId"] = new SelectList(_context.Set<Skill>(), "Id", "Id");
            return View();
        }

        // POST: CompetitorSkill/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CompetitorId,SkillId")] CompetitorSkill competitorSkill)
        {
            if (ModelState.IsValid)
            {
                _context.Add(competitorSkill);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompetitorId"] = new SelectList(_context.Competitor, "Id", "Id", competitorSkill.CompetitorId);
            ViewData["SkillId"] = new SelectList(_context.Set<Skill>(), "Id", "Id", competitorSkill.SkillId);
            return View(competitorSkill);
        }

        // GET: CompetitorSkill/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var competitorSkill = await _context.CompetitorSkill.FindAsync(id);
            if (competitorSkill == null)
            {
                return NotFound();
            }
            ViewData["CompetitorId"] = new SelectList(_context.Competitor, "Id", "Id", competitorSkill.CompetitorId);
            ViewData["SkillId"] = new SelectList(_context.Set<Skill>(), "Id", "Id", competitorSkill.SkillId);
            return View(competitorSkill);
        }

        // POST: CompetitorSkill/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CompetitorId,SkillId")] CompetitorSkill competitorSkill)
        {
            if (id != competitorSkill.CompetitorId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(competitorSkill);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CompetitorSkillExists(competitorSkill.CompetitorId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompetitorId"] = new SelectList(_context.Competitor, "Id", "Id", competitorSkill.CompetitorId);
            ViewData["SkillId"] = new SelectList(_context.Set<Skill>(), "Id", "Id", competitorSkill.SkillId);
            return View(competitorSkill);
        }

        // GET: CompetitorSkill/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var competitorSkill = await _context.CompetitorSkill
                .Include(c => c.Competitor)
                .Include(c => c.Skill)
                .FirstOrDefaultAsync(m => m.CompetitorId == id);
            if (competitorSkill == null)
            {
                return NotFound();
            }

            return View(competitorSkill);
        }

        // POST: CompetitorSkill/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var competitorSkill = await _context.CompetitorSkill.FindAsync(id);
            _context.CompetitorSkill.Remove(competitorSkill);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CompetitorSkillExists(int id)
        {
            return _context.CompetitorSkill.Any(e => e.CompetitorId == id);
        }
    }
}
