namespace ASPCompetitionManager.Models
{
    public class Skill
    {
        public int Id { get; set; }
        public Group Group { get => (Group) Id; }
        public string Name { get => StringValue.GetStringValue(Group); }
    }
}