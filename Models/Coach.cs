namespace ASPCompetitionManager.Models
{
    public class Coach : Person
    {
        public int TrainingModifier { get; set; }
    }
}