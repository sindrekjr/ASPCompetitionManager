﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ASPCompetitionManager.Migrations
{
    public partial class CreateDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Coach",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Gender = table.Column<int>(nullable: false),
                    Age = table.Column<int>(nullable: false),
                    TrainingModifier = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Coach", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Skill",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Skill", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Team",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Team", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Competitor",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Gender = table.Column<int>(nullable: false),
                    Age = table.Column<int>(nullable: false),
                    CoachId = table.Column<int>(nullable: false),
                    TeamId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Competitor", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Competitor_Coach_CoachId",
                        column: x => x.CoachId,
                        principalTable: "Coach",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Competitor_Team_TeamId",
                        column: x => x.TeamId,
                        principalTable: "Team",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CompetitorSkill",
                columns: table => new
                {
                    CompetitorId = table.Column<int>(nullable: false),
                    SkillId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompetitorSkill", x => new { x.CompetitorId, x.SkillId });
                    table.ForeignKey(
                        name: "FK_CompetitorSkill_Competitor_CompetitorId",
                        column: x => x.CompetitorId,
                        principalTable: "Competitor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CompetitorSkill_Skill_SkillId",
                        column: x => x.SkillId,
                        principalTable: "Skill",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Coach",
                columns: new[] { "Id", "Age", "FirstName", "Gender", "LastName", "TrainingModifier" },
                values: new object[,]
                {
                    { 1, 0, "Steve", 0, null, 7 },
                    { 2, 0, "Atlas", 0, null, 7 },
                    { 3, 0, "Greg", 0, null, 5 },
                    { 4, 0, "Dean", 0, null, 7 }
                });

            migrationBuilder.InsertData(
                table: "Skill",
                column: "Id",
                values: new object[]
                {
                    1,
                    2,
                    3,
                    4,
                    5,
                    6,
                    7,
                    8
                });

            migrationBuilder.InsertData(
                table: "Team",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Team Good" },
                    { 2, "Team Evil" }
                });

            migrationBuilder.InsertData(
                table: "Competitor",
                columns: new[] { "Id", "Age", "CoachId", "FirstName", "Gender", "LastName", "TeamId" },
                values: new object[,]
                {
                    { 2, 0, 3, "Solid Snake", 1, null, 1 },
                    { 4, 0, 1, "Jim", 1, "Halpert", 1 },
                    { 6, 0, 3, "Abysswalker Artorias", 1, null, 1 },
                    { 8, 0, 1, "Farah", 2, null, 1 },
                    { 10, 0, 3, "Lucatiel", 2, null, 1 },
                    { 12, 0, 1, "Caelar", 2, "Argent", 1 },
                    { 14, 0, 3, "Bellatrix", 2, "Lestrange", 1 },
                    { 1, 0, 2, "Gordon", 1, "Freeman", 2 },
                    { 3, 0, 4, "Maximus", 1, "Decimus Meridius", 2 },
                    { 5, 0, 2, "Sander", 1, "Cohen", 2 },
                    { 7, 0, 4, "Sekiro", 1, null, 2 },
                    { 9, 0, 2, "Alyx", 2, "Vance", 2 },
                    { 11, 0, 4, "Quelana", 2, null, 2 },
                    { 13, 0, 2, "Olishilelia", 2, null, 2 }
                });

            migrationBuilder.InsertData(
                table: "CompetitorSkill",
                columns: new[] { "CompetitorId", "SkillId" },
                values: new object[,]
                {
                    { 2, 8 },
                    { 1, 8 },
                    { 3, 2 },
                    { 3, 6 },
                    { 3, 8 },
                    { 5, 5 },
                    { 5, 2 },
                    { 5, 1 },
                    { 7, 1 },
                    { 7, 6 },
                    { 7, 4 },
                    { 9, 3 },
                    { 9, 6 },
                    { 9, 1 },
                    { 11, 3 },
                    { 11, 4 },
                    { 11, 2 },
                    { 13, 2 },
                    { 1, 2 },
                    { 1, 6 },
                    { 14, 4 },
                    { 14, 3 },
                    { 2, 5 },
                    { 2, 3 },
                    { 4, 3 },
                    { 4, 4 },
                    { 4, 8 },
                    { 6, 7 },
                    { 6, 3 },
                    { 6, 4 },
                    { 13, 7 },
                    { 8, 2 },
                    { 8, 8 },
                    { 10, 1 },
                    { 10, 2 },
                    { 10, 4 },
                    { 12, 3 },
                    { 12, 1 },
                    { 12, 8 },
                    { 14, 6 },
                    { 8, 1 },
                    { 13, 4 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Competitor_CoachId",
                table: "Competitor",
                column: "CoachId");

            migrationBuilder.CreateIndex(
                name: "IX_Competitor_TeamId",
                table: "Competitor",
                column: "TeamId");

            migrationBuilder.CreateIndex(
                name: "IX_CompetitorSkill_SkillId",
                table: "CompetitorSkill",
                column: "SkillId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CompetitorSkill");

            migrationBuilder.DropTable(
                name: "Competitor");

            migrationBuilder.DropTable(
                name: "Skill");

            migrationBuilder.DropTable(
                name: "Coach");

            migrationBuilder.DropTable(
                name: "Team");
        }
    }
}
