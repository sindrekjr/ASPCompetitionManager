using Microsoft.EntityFrameworkCore;

namespace ASPCompetitionManager.DAL
{
    public class CompetitionContext : DbContext
    {

        public CompetitionContext() : base() {}

        public CompetitionContext(DbContextOptions<CompetitionContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder) => CompetitionSeed.Seed(modelBuilder);

        public DbSet<ASPCompetitionManager.Models.Competitor> Competitor { get; set; }

        public DbSet<ASPCompetitionManager.Models.Coach> Coach { get; set; }

        public DbSet<ASPCompetitionManager.Models.CompetitorSkill> CompetitorSkill { get; set; }

        public DbSet<ASPCompetitionManager.Models.Skill> Skill { get; set; }

        public DbSet<ASPCompetitionManager.Models.Team> Team { get; set; }
    }
}